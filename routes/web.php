<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/tasks');
Route::put('/tasks/{task}/update-status', [TaskController::class, 'updateStatusOnBoard']);

Route::resource('tasks', TaskController::class);
Route::get('/tasks/{task}/subtask/create', [TaskController::class, 'createSubtask'])->name('tasks.createSubtask');
Route::post('/tasks/{task}/subtask', [TaskController::class, 'storeSubtask'])->name('tasks.storeSubtask');
Route::get('/tasks/{task}/subtask/{subtask}/edit', [TaskController::class, 'editSubtask'])->name('tasks.editSubtask');
Route::put('/tasks/{task}/subtask/{subtask}', [TaskController::class, 'updateSubtask'])->name('tasks.updateSubtask');
Route::delete('/tasks/{task}/subtask/{subtask}', [TaskController::class, 'destroySubtask'])->name('tasks.destroySubtask');
Route::get('/tasks/{taskId}/subtasks/{subtaskId}', [TaskController::class, 'showSubtask'])->name('subtasks.show');