@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="text-center mt-4 mb-4">Создать новую задачу</h1>

        <form method="post" action="{{ route('tasks.store') }}">
            @csrf

            <div class="form-group">
                <label for="title">Заголовок</label>
                <input type="text" class="form-control" id="title" name="title" required>
            </div>

            <div class="form-group">
                <label for="description">Описание</label>
                <textarea class="form-control" id="description" name="description" rows="3"></textarea>
            </div>

            <div class="form-group">
                <label for="date">Дата</label>
                <input type="date" class="form-control" id="date" name="date" required>
            </div>

            <div class="form-group">
                <label for="status">Статус</label>
                <select class="form-control" id="status" name="status" required>
                    <option value="pending">Ожидающая</option>
                    <option value="in_progress">В процессе</option>
                    <option value="completed">Завершенная</option>
                </select>
            </div>

            <button type="submit" class="btn btn-primary">Создать задачу</button>
        </form>
    </div>
@endsection
