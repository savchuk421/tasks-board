@extends('layouts.app')

@section('content')
    <div class="container mt-4">
        <form action="{{ route('tasks.index') }}" method="get" class="mb-4">
            <div class="row align-items-end">
                <div class="col-md-6">
                    <label for="title" class="form-label">Заголовок:</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="title" name="title" value="{{ request('title') }}">
                        <button type="submit" class="btn btn-primary">Искать</button>
                    </div>
                </div>

                <div class="col-md-6">
                    <label for="status" class="form-label">Статус:</label>
                    <select class="form-select" id="status" name="status">
                        <option value="" selected>Все статусы</option>
                        <option value="pending" {{ request('status') === 'pending' ? 'selected' : '' }}>В ожидании</option>
                        <option value="in_progress" {{ request('status') === 'in_progress' ? 'selected' : '' }}>В процессе</option>
                        <option value="completed" {{ request('status') === 'completed' ? 'selected' : '' }}>Завершено</option>
                    </select>
                </div>
            </div>
        </form>
        <div class="row">
            @foreach(['pending', 'in_progress', 'completed'] as $status)
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header bg-secondary text-white">
                            <h2 class="text-center">{{ ucfirst($status) }}</h2>
                        </div>
                        <div class="card-body">
                            <ul class="list-group task-list" data-status="{{ $status }}">
                                @forelse($tasks->where('status', $status) as $task)
                                    <li class="list-group-item task-item" data-id="{{ $task->id }}">
                                        <h4>{{ $task->title }}</h4>
                                        <p><strong>Дата:</strong> {{ $task->date }}</p>
                                        <p><strong>Описание:</strong> {{ $task->description ?? 'Нет описания' }}</p>
                                        <h5>Подзадачи:</h5>
                                        @forelse($task->subtasks as $subtask)
                                            <p>{{ $subtask->title }} - {{ $subtask->status }}</p>
                                        @empty
                                            <p>Нет подзадач</p>
                                        @endforelse

                                        <div class="float-right">
                                            <a href="{{ route('tasks.createSubtask', $task->id) }}" class="btn btn-sm btn-primary" title="Создать подзадачу">
                                                <i class="fas fa-plus"></i>
                                            </a>
                                            <a href="{{ route('tasks.edit', $task->id) }}" class="btn btn-sm btn-warning" title="Редактировать">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            <form method="post" action="{{ route('tasks.destroy', $task->id) }}" class="d-inline">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="btn btn-sm btn-danger" title="Удалить">
                                                    <i class="fas fa-trash-alt"></i>
                                                </button>
                                            </form>
                                        </div>
                                    </li>
                                @empty
                                    <li class="list-group-item text-muted">Нет задач</li>
                                @endforelse
                            </ul>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="text-center mt-4">
            <a href="{{ route('tasks.create') }}" class="btn btn-success" title="Создать новую задачу">
                <i class="fas fa-plus"></i> Создать новую задачу
            </a>
        </div>
    </div>

    <script>
        // Инициализация Sortable.js для каждого списка
        document.addEventListener('DOMContentLoaded', function () {
            var lists = document.querySelectorAll('.task-list');
            lists.forEach(function (list) {
                new Sortable(list, {
                    group: 'tasks',
                    animation: 150,
                    onEnd: function (evt) {
                        var taskId = evt.item.dataset.id;
                        var newStatus = evt.to.dataset.status;

                        // Отправляем запрос на обновление статуса в базе данных
                        fetch(`/tasks/${taskId}/update-status`, {
                            method: 'PUT',
                            headers: {
                                'Content-Type': 'application/json',
                                'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content
                            },
                            body: JSON.stringify({
                                status: newStatus
                            })
                        });
                    }
                });
            });
        });
    </script>
@endsection
