@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="text-center mt-4 mb-4">{{ $task->title }}</h1>
        <div class="card">
            <div class="card-body">
                <p class="card-text">{{ $task->description }}</p>
                <p class="card-text"><strong>Status:</strong> {{ $task->status }}</p>
                <p class="card-text"><strong>Date:</strong> {{ $task->date }}</p>

                <a href="{{ route('tasks.edit', $task->id) }}" class="btn btn-warning">Редактировать задачу</a>
            </div>
        </div>

        <h2 class="mt-4">Подзадачи</h2>
        @forelse($task->subtasks as $subtask)
            <div class="card mt-2">
                <div class="card-body">
                    <p class="card-text">{{ $subtask->title }} - {{ $subtask->status }}</p>
                </div>
            </div>
        @empty
            <p class="mt-2">Подзадач нет</p>
        @endforelse
    </div>
@endsection
