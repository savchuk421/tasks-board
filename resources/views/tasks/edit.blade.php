@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="mt-4 mb-4">Редактирование задачи</h1>
        <form action="{{ route('tasks.update', $task->id) }}" method="post">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Заголовок:</label>
                <input type="text" class="form-control" name="title" id="title" value="{{ $task->title }}" required>
            </div>
            <div class="form-group">
                <label for="description">Описание:</label>
                <textarea class="form-control" name="description" id="description">{{ $task->description }}</textarea>
            </div>
            <div class="form-group">
                <label for="date">Дата:</label>
                <input type="date" class="form-control" name="date" id="date" value="{{ $task->date }}" required>
            </div>
            <div class="form-group">
                <label for="status">Статус:</label>
                <select class="form-control" name="status" id="status" required>
                    <option value="pending" {{ $task->status === 'pending' ? 'selected' : '' }}>В ожидании</option>
                    <option value="in_progress" {{ $task->status === 'in_progress' ? 'selected' : '' }}>В процессе</option>
                    <option value="completed" {{ $task->status === 'completed' ? 'selected' : '' }}>Завершено</option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Обновить задачу</button>
        </form>
        <a href="{{ route('tasks.index') }}" class="btn btn-secondary mt-2">Назад к списку задач</a>
    </div>
@endsection
