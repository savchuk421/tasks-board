@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="text-center mt-4 mb-4">{{ $subtask->title }}</h1>
        <div class="card">
            <div class="card-body">
                <p class="card-text">{{ $subtask->description }}</p>
                <p class="card-text"><strong>Status:</strong> {{ $subtask->status }}</p>
                <p class="card-text"><strong>Date:</strong> {{ $subtask->created_at->format('d.m.Y H:i') }}</p>

                <a href="{{ route('tasks.editSubtask', ['task' => $subtask->task_id, 'subtask' => $subtask->id]) }}" class="btn btn-warning">Редактировать подзадачу</a>
            </div>
        </div>
    </div>
@endsection
