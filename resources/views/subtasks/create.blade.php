@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="text-center mt-4 mb-4">Создать новую подзадачу для задачи "{{ $task->title }}"</h1>

        <form action="{{ route('tasks.storeSubtask', $task->id) }}" method="post">
            @csrf

            <div class="form-group">
                <label for="title">Заголовок</label>
                <input type="text" class="form-control" id="title" name="title" required>
            </div>

            <div class="form-group">
                <label for="description">Описание</label>
                <textarea class="form-control" id="description" name="description" rows="3"></textarea>
            </div>

            <div class="form-group">
                <label for="status">Статус</label>
                <select class="form-control" id="status" name="status" required>
                    <option value="pending">В ожидании</option>
                    <option value="in_progress">В процессе</option>
                    <option value="completed">Завершено</option>
                </select>
            </div>

            <button type="submit" class="btn btn-primary">Создать подзадачу</button>
        </form>

        <a href="{{ route('tasks.show', $task->id) }}" class="btn btn-secondary mt-3">Назад к задаче</a>
    </div>
@endsection

