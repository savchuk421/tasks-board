@extends('layouts.app')

@section('content')
    <h1>Список подзадач для задачи: {{ $task->title }}</h1>
    <ul>
        @forelse($task->subtasks as $subtask)
            <li>
                {{ $subtask->title }} - {{ $subtask->status }}
                <a href="{{ route('tasks.editSubtask', ['task' => $task->id, 'subtask' => $subtask->id]) }}">Редактировать подзадачу</a>
            </li>
        @empty
            <li>Нет подзадач</li>
        @endforelse
    </ul>
    <a href="{{ route('tasks.show', $task->id) }}">Назад к задаче</a>
@endsection
