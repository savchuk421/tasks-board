@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="mt-4 mb-4">Редактирование подзадачи</h1>
        <form action="{{ route('tasks.updateSubtask', [$task->id, $subtask->id]) }}" method="post">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Заголовок:</label>
                <input type="text" class="form-control" name="title" id="title" value="{{ $subtask->title }}" required>
            </div>
            <div class="form-group">
                <label for="description">Описание:</label>
                <textarea class="form-control" name="description" id="description">{{ $subtask->description }}</textarea>
            </div>
            <div class="form-group">
                <label for="status">Статус:</label>
                <select class="form-control" name="status" id="status" required>
                    <option value="pending" {{ $subtask->status === 'pending' ? 'selected' : '' }}>В ожидании</option>
                    <option value="in_progress" {{ $subtask->status === 'in_progress' ? 'selected' : '' }}>В процессе</option>
                    <option value="completed" {{ $subtask->status === 'completed' ? 'selected' : '' }}>Завершено</option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Обновить подзадачу</button>
        </form>
        <a href="{{ route('tasks.show', $task->id) }}" class="btn btn-secondary mt-2">Назад к задаче</a>
    </div>
@endsection
