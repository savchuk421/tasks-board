<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;
use App\Models\Subtask;
use App\Models\TelegramModel;
use Illuminate\Support\Facades\Config;

class TaskController extends Controller
{
    public function index(Request $request)
    {
        $tasks = Task::query();

        if ($request->filled('title')) {
            $tasks->where('title', 'like', '%' . $request->input('title') . '%');
        }

        if ($request->filled('status')) {
            $tasks->where('status', $request->input('status'));
        }

        $filteredTasks = $tasks->get();

        return view('tasks.index', ['tasks' => $filteredTasks]);
    }

    public function create()
    {
        return view('tasks.create');
    }

    public function store(Request $request, TelegramModel $telegram)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'nullable|string',
            'date' => 'required|date',
            'status' => 'required|in:pending,in_progress,completed',
        ]);

        // Создание новой задачи в базе данных
        $newTask = Task::create([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'date' => $request->input('date'),
            'status' => $request->input('status'),
        ]);

        $chatId = env('TELEGRAM_CHANNEL_ID');
        $text = 'Новая задача создана: ' . $newTask->title;
        $telegram->sendMessage($chatId, $text);

        return redirect()->action([TaskController::class, 'index']);
    }

    public function edit(Task $task)
    {
    }

    public function update(Request $request, Task $task)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'nullable|string',
            'date' => 'required|date',
            'status' => 'required|in:pending,in_progress,completed',
        ]);

        $task->update([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'date' => $request->input('date'),
            'status' => $request->input('status'),
        ]);

        return redirect('/tasks');
    }

    public function updateStatusOnBoard(Request $request, Task $task)
    {
        $request->validate([
            'status' => 'required|in:pending,in_progress,completed',
        ]);

        $task->update([
            'status' => $request->input('status'),
        ]);

        return redirect('/tasks');
    }

    public function destroy(Task $task)
    {
        $task->delete();

        return redirect('/tasks');
    }

    public function show(Task $task)
    {
        return view('tasks.show', ['task' => $task]);
    }

    public function createSubtask(Task $task)
    {
        return view('subtasks.create', ['task' => $task]);
    }

    public function editSubtask(Task $task, Subtask $subtask)
    {
        return view('subtasks.edit', ['task' => $task, 'subtask' => $subtask]);
    }

    public function updateSubtask(Request $request, Task $task, Subtask $subtask)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'nullable|string',
            'status' => 'required|in:pending,in_progress,completed',
        ]);

        $subtask->update([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'status' => $request->input('status'),
        ]);

        return redirect('/tasks/'.$task->id.'/subtasks/'.$subtask->id);
    }

    public function storeSubtask(Request $request, Task $task, TelegramModel $telegram)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'nullable|string',
            'status' => 'required|in:pending,in_progress,completed',
        ]);

        $newSubtask = $task->subtasks()->create([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'status' => $request->input('status'),
        ]);

        $chatId = env('TELEGRAM_CHANNEL_ID');
        $text = 'Новая подзадача к задаче <' . $task->title .  '> создана: ' . $newSubtask->title;
        $telegram->sendMessage($chatId, $text);

        return redirect('/tasks/'.$task->id);
    }

    public function destroySubtask(Task $task, Subtask $subtask)
    {
        $subtask->delete();

        return redirect('/tasks/'.$task->id);
    }

    public function showSubtask($taskId, $subtaskId)
    {
        $subtask = Subtask::findOrFail($subtaskId);

        return view('subtasks.show', compact('subtask'));
    }
}