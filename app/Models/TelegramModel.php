<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Telegram\Bot\Laravel\Facades\Telegram;

class TelegramModel extends Model
{
    public function sendMessage($chatId, $text)
    {
        try {
            Telegram::sendMessage([
                'chat_id' => $chatId,
                'text' => $text,
            ]);
            
            return true;
        } catch (\Exception $e) {
            \Log::error('Telegram message sending failed: ' . $e->getMessage());

            return false;
        }
    }
}